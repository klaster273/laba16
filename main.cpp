#include <iostream>
#include <ctime>
#include <vector>

//Standart C++20 using, test programm

using namespace std;

int getRandomNumber(int min, int max)
{
    static const double fraction = 1.0 / (static_cast<double>(RAND_MAX) + 1.0);

    return static_cast<int>(rand() * fraction * (max - min + 1) + min);
}

int main() {

    int counter, max, min;
    short custom_size;

    srand(time(NULL));


    cout << "Input size of your custom vector: ";
    cin >> custom_size;
    cout << endl;

    vector<int>standart20_test_vector(custom_size);

    cout << "Input max and min element of vector: ";
    cin >> max >> min;
    cout << endl;

    standart20_test_vector[0] = min;
    standart20_test_vector[1] = max;

    //Input vector with rand function
    for (counter = 2; counter < custom_size; counter++){
        standart20_test_vector[counter] = getRandomNumber(min, max);
    }

    //Cout not sorted vector
    for (counter = 0; counter < custom_size; counter++){
        cout << "Element " << counter << " " << standart20_test_vector[counter] << endl;
    }

    cout << endl;
    cout << "Now we sort vector!" << endl;
    cout << endl;

    //Bootle sorted vector
    for (counter = 0; counter < custom_size; counter++)
    {
        int sort_use = standart20_test_vector[counter];
        for (int counter_2 = counter; counter_2 < custom_size; counter_2++)
            if (sort_use > standart20_test_vector[counter_2])
            {
                sort_use = standart20_test_vector[counter_2];
                standart20_test_vector[counter_2] = standart20_test_vector[counter];
                standart20_test_vector[counter] = sort_use;
            }
    }

    for (counter = 0; counter < custom_size; counter++){
        cout << "Sorted vector element " << counter << " " << standart20_test_vector[counter] << endl;
    }

    return 0;
}
